/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.team142.vroom.data;

import java.util.ArrayList;

/**
 *
 * @author just1689
 */
public class Book {
    
    private ArrayList<String> words;
    private int size;
    private int pos;
    
    public Book(ArrayList<String> data) {
        reset(data);
    }
    
    public String getNext() {
        pos++;
        if (pos < size)
            return words.get(pos);
        
        pos = size;
        return "/quit";
    }
    
    public int getSize() {
        return words.size();
    }
    
    public void reset() {
        size = words.size();
        pos = 0;
    }
    
    public void reset(ArrayList<String> data) {
        words = data;
        reset();
    }
    
    public void rewind(int words) {
        pos = pos - words;
        if (pos < 0)
            pos = 0;
    }
    
}
