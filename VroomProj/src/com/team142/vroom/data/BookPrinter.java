/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.team142.vroom.data;

import java.util.ArrayList;

/**
 *
 * @author just1689
 */
public class BookPrinter {
    public static ArrayList<String> printBook(String text) {
        ArrayList<String> rows = new ArrayList<String>();
        char c;
        String word = "";
        for (int i = 0; i < text.length(); i++) {
            c = text.charAt(i);
            if (c == '\n') {
                rows.add(word);
                rows.add("   ");
                word = "";
            } else if (c == ' ') {
                rows.add(word);
                word = "";
            } else {
                word += c;
            }
        }
        return rows;
    }
}
