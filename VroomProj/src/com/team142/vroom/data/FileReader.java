/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.team142.vroom.data;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author just1689
 */
public class FileReader {
    public static String readFile(String filename) throws IOException  {
        RandomAccessFile myFile;
        myFile = new RandomAccessFile(filename, "r");
        byte[] myFileBytes = new byte[(int)myFile.length()];
        myFile.read(myFileBytes);
        myFile.close();        

        return new String(myFileBytes);
    }
    
}
